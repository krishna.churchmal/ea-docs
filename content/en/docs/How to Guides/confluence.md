---
title: 'Elusium Free-Trail'
description: "This is the description of Confulence Test 1."
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "How to Guides"
weight: 100
toc: true
---


## Elysium Free-Trial Sign-up Process Steps:

1. Go to URLhttps://trial.elysiumanalytics.ai/ and click on “TRY FOR FREE” as shown below

![ElysiumOverview](/Picture1.png)

2.	Fill the form with appropriate details and click on “START MY FREE TRIAL”
    1.	First Name: Your First Name
    2.	Last Name: Your Last Name
    3.	Email: valid Email ID which will be used for activation and for further communication
    4.	Country: Your Country
    5.	Cloud Provider: You can select any one of the options

    ![ElysiumOverview](/Picture2.png)
 
3.	You will see the successful registration message as below. Wait for the email from Elysium to the Email ID mentioned while filling the form for your account activation

![ElysiumOverview](/Picture3.png)
 
4.	You will b receiving an email from system@elysiumanalytics.ai as shown below

![ElysiumOverview](/Picture4.png)
 
5.	Click on the “Activate now” in the email or copy the URL from the link and open in the browser
6.	Opening the URL will take you to page where you can set password with your registered Email ID. (Note: Make sure the Email ID shown is same as used while registration). Enter your preferred password and confirm password. Then click on “SIGN UP” button at bottom of page

![ElysiumOverview](/Picture5.png)
 
7.	You will be taken to login page of Elysium. Provide “User Name” with your registered Email ID, Password as set above and click on “LOGIN”

![ElysiumOverview](/Picture6.png)
 
8.	Now you will be taken to process where Snowflake account need to be setup. There are two options. One is to choose “Elysium Hosted Account” which means, Elysium Snowflake account will be used during your trial period with limited access. Second option is “Your Snowflake Account” which means, you can use your existing snowflake account or create a snowflake trail account and proceed
    1.	Option “Elysium Hosted Account” steps
        1.	Select “Elysium Hosted Account” and click on “NEXT”

        ![ElysiumOverview](/Picture7.png)
        
        2.	Necessary database setup is done in the backend. On successful setup, you will see the below screen. To proceed further click on “CONNECT“

        ![ElysiumOverview](/Picture8.png)
        
        3.	In few moments you will be able to see the below screen and you are ready to get started with Elysium

        ![ElysiumOverview](/Picture9.png)
 
    2.	Option “Your Snowflake Account“ steps
        1.	Select “Your Snowflake Account“ option as below

        ![ElysiumOverview](/Picture10.png)
        
        2.	Provide your Snowflake account and snowflake URL 

        ![ElysiumOverview](/Picture11.png)
        
        3.	SnowSQL script is provided as code snippet, which need to be copied and executed in your snowflake account with Account Admin Privileges.
            1.	Click the copy button to copy the SQL snippet as shown below

            ![ElysiumOverview](/Picture12.png)
            
            2.	Login to your snowflake account with user who have Account Admin privileges
            3.	Open new worksheet in your snowflake after login and paste the SQL code snippet
            4.	Execute the complete SQL snippet in the worksheet. Note: Make sure the SQL snippet is execute with user having Account Admin Privileges 
        4.	After successful execution of complete snippet in your snowflake account. Click on “VALIDATE & GO“ 

        ![ElysiumOverview](/Picture13.png)
 
        5.	If all the steps are successful, you will see the below screen where you can click on “CONNECT“ button to proceed further
        
        ![ElysiumOverview](/Picture14.png)
 
        6.	In few moments you will be able to see the below screen and you are ready to get started with Elysium

        ![ElysiumOverview](/Picture15.png)
 