---
title: '1. Introduction'
description: "This is the description of Introduction of Elysium Analytics Workshop V2."
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "Elysium V2"
weight: 100
toc: true
---

## Introduction

Welcome to Elysium Analytics (EA). This workshop is a first in the series of workshops intended to introduce you to some of Elysium core capabilities. We will explain how Elysium can be used as SIEM solution using Elysium Search app. Once we cover the basics, you'll be ready to start processing your own data and diving into more advanced features of Elysium Analytics.

Elysium Cloud security analytics platform is Data-Driven Security Analytics with contextualized data lake of full text, semantic search and visibility into all critical corporate assets to ensure that information is safe from compromise and insider threats.

### Problem Statement:
Nowadays, large IT enterprises are dealing with massive amount of events from thousands servers and services. Regulatory compliance and internal regulations demand long-term storage for these events as well as ability for on-demand security analysis. With ever increasing event generation sources it becomes hard to perform cross-source security assessment. There is a need for single solution that can tie together events from servers, endpoints and processes to confront internal inefficiencies and provide integrated, robust and sophisticated approach to operations and security analysis.

In a time of increasing data volumes and the heightened need for data governance and security, legacy solutions are challenged with scaling to current telemetry data volume from cloud applications, cloud infrastructure, and endpoint solutions. Security operations need a security data lake that brings security telemetry into a unified taxonomy for a single source of truth to detect and help understand threats more effectively with richer context.

### Why Elysium Analytics:
The Elysium Analytics Security Data Lake solution is the first fully operationalized, turnkey security data lake with an open data model for contextual and deep analytics natively built on Snowflake. To give customers immediate value, Elysium Analytics also provides pre-built search and analytics applications for threat investigations, threat hunting, and threat monitoring. Additionally, customers can build their own unique use cases and applications based on their data needs.

Elysium provides all the essential applications and capabilities required for in-depth analytics of escalated cases in the SOC: in-depth analysis, advanced ML-based analytics, Graph analytics, correlations, full-text search, visualization, trending, historical analysis, and compliance reporting. These applications are natively enabled on the data lake and are ready for use from day one. Elysium solution is cloud scale with elastic compute and unlimited low-cost storage billed on a usage basis, providing better performance at any load at a far lower TCO than legacy solutions.

* Cloud Native running on Snowflake
* Semantic data lake with open data model enables organizations to perform contextual analytics and full text search across all data sources
* Machine learning based log analysis solution for security minded enterprises from mid to large sized
* Offered on a usage basis lowering long-term storage cost and reduces financial risk from incoming events spikes
* Open platform with no vendor lock-in, built-in analytics models with APIs for end user development
* A single pane of glass for a SOC analyst that helps identify and observe aggregated views of all user activities and entities across an enterprise
* Open security framework deployments with add-ons that utilize analytics created by security framework professionals
* Engineers have access to the all telemetry required for applications performance improvement and downtime minimization and cost optimization
* View more at out website: Elysium Site and Elysium Overview represented as follows
introduction

![Intro pic](/EV2-1.png)

### The goal of this workshop is to enable you to:
* Ingest multiple service logs supported by Elysium Open Data Model(EODM)
* Investigate security logs using Elysium SIEM solutions
* Perform real-time analytics of security events and threat detection
* Visualize data with the Elysium Dashboards (Kibana), and use graph analytics
* Create custom Elysium alerts with notifications

### Prerequisites:
* Basic database concepts understanding and knowledge of SQL
* Basic knowledge on AWS services and its security logs

### Who is this workshop for:
* InfoSec and DevSecOps Professionals
* IT Managers and Architects

### What You'll Learn:
* How to load, ingest, parse, store users related data using Elysium Open Data Model
* How to reduce users operational configuration risks using Elysium Cloud data connectors
* How to get all telemetry data from endpoints in an organization
* How to perform analytical queries on data using Elysium search and query workbench
* How to minimize cost and reduce the risks
* How to monitor solutions that aggregate data from security sources such as applications,infrastructure,networking etc
* How to identify anomolous behaviour using machine learning algorithms
* How to do real-time analytics of security events and threat detection
* How to create custom alerts and send notifications alert
* How to visualize the infrastructure endpoints using graph analytics