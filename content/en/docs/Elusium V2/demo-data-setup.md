---
title: '3. Demo Data Setup'
description: "This is the description of Demo Data Setup of Elysium Analytics Workshop V2."
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "Elysium V2"
weight: 100
toc: true
---

## Demo Data Setup
In this workshop we will configure and pre-loaded three AWS data sources into Snowflake data lake to showcase the basic functionality of Elysium SAAS apps. Please note that there are EA Connector capabilities with Elysium Analytics where users can configure services to collect data various sources such as AWS, Azure, On-Prem.

### Amazon GuardDuty
Amazon GuardDuty is a threat detection service that continuously monitors malicious behavior to protect your AWS accounts and workloads.

### AWS Cloud Trail Logs
AWS Cloud Trail Logs captures history of every API call made to any resource or service in an AWS account.

### Amazon VPC Flow
Amazon VPC Flow is a service that enables you to capture information about the IP traffic going to and from network interfaces in your VPC.

* Client can load custom data to Elysium platform based on their needs.
* Amazon GuardDuty Logs contain fields to identify unexpected and potentially unauthorized or malicious activities within your AWS environment from compromised EC2 Instances. The sample log contains fields like:

| Field Name | Description |
| ---- | ---- |
| @log_type | Log type or service that sent the log |
| rule.name | Detection name such as IDS, Inspector and Macie detection result names |
| source.ip | Source IP address |
| user.id | The unique ID for the user or access key for AWS resources |
| user.name | Username for AWS resources |
| cloud.instance.id | EC2 Instance ID |
| rule.description | Description of detected threats |

