---
title: '2. Environment Setup'
description: "This is the description of Environment Setup of Elysium Analytics Workshop V2."
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "Elysium V2"
weight: 100
toc: true
---

## Environment Setup
Elysium Analytics being a Cloud based SaaS application, there are no specific installations necessary. You will only need to understand the logs that are collected from AWS set up for demo.

### Request for a Demo:
To book a demo, the Client form can be accessed here: Fill the form
For Elysium Sales related inquiries: sales@elysiumanalytics.ai
### Get free trial of Elysium:
After filling free trial details of Elysium Product at Free Trail, client will get credentials and access management roles to start using EA features 2.0 demo setup at Elysium Demo 2.0
If you want to whitelist our Elysium SaaS App IP, please reach out to: support@elysiumanalytics.ai
### Workshop Flow
1. Introduction
* Workshop Goals
* Prerequisites
* What You'll Learn
2. Environment Setup
* Request for a Demo
* Get free trial of Elysium
* Workshop Flow
3. Demo Data Setup
* Amazon GuardDuty
* AWS Cloud Trail Logs
* Amazon VPC Flow
4. Log Analysis
* Discover screen
* How to Search
* Specify Time Range
* Log Selection
* Log Fields
* Time series display
* Normalized fields
* Walkthrough
* Summary of Log analysis
5. Analytics Dashboard
* Create a Dashboard
* Create a Visualizations
6. Alerts Management
* Setup new alert rule
* Configure alert properties
* Configure notification settings
* Configure Alerts based on saved search
* View generated alerts
7. Advanced Log Analysis
* Start Investigation
* AWS IAM Credential Infringement
* Compromised EC2 Instances
* Compromised S3 Bucket
8. Elysium Analytics Full-text Search Snowflake App
* Elysium Analytics Full-Text Search
* Installing Elysium Analytics Full-Text Search app
* Things to be known before using the app
* Running Full-Text Searches and Use cases