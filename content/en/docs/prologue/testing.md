---
title: "Nishanth Test"
description: "this is description of Nishanth Test."
lead: "this is lead of Nishanth Test."
date: 2020-11-16T13:59:39+01:00
lastmod: 2020-11-16T13:59:39+01:00
draft: false
images: []
menu:
  docs:
    parent: "prologue"
weight: 110
toc: true
---


## this is a main heading 1

lolo js dfvseb vjv cejahv ea vj evc eaguc ehk v vaeur vaue rvu qera gvoueg rvuqe rgvu 

### sub heading 1-1

lolo js dfvseb vjv cejahv ea vj evc eaguc ehk v vaeur vaue rvu qera gvoueg rvuqe rgvu lolo js dfvseb vjv cejahv ea vj evc eaguc ehk v vaeur vaue rvu qera gvoueg rvuqe rgvu 

### sub sub heading 1-1-1

- Ilolo js dfvseb vjv cejahv ea vj evc eaguc ehk v vaeur vaue rvu qera gvoueg rvuqe rgvu 
- Intended for minor customizations
- [Easily update npm packages]({{< relref "how-to-update" >}}) — __including__ [Doks](https://www.npmjs.com/package/@hyas/doks)

```bash
git clone https://github.com/h-enk/doks-child-theme.git my-doks-site
```

#### Starter theme

- Intended for intermediate to advanced users
- Intended for major customizations
- [Easily update npm packages]({{< relref "how-to-update" >}})

```bash
git clone https://github.com/h-enk/doks.git my-doks-site
```

{{< details "Help me choose" >}}
Not sure which one is for you? Pick the child theme.
{{< /details >}}

### Change directories

```bash
cd my-doks-site
```

### Install dependencies

```bash
npm install
```

### Start development server

```bash
npm run start
```

Doks will start the Hugo development webserver accessible by default at `http://localhost:1313`. Saved changes will live reload in the browser.

## Other commands

Doks comes with commands for common tasks. [Commands →]({{< relref "commands" >}})
